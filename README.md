## RAPIDS Notebooks HPE & NVIDIA DEEP LEARNING WORKSHOP (Industrial Quality Inspection)

#### List of Notebooks:

| Folder    | Notebook Title         | Description                                                                                                                                                                                                                   | GPU  | Dataset Used
|-----------|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----|----|
|    | [Copurchase-network-analysis](cuGraph/Copurchase-network-analysis.ipynb)           | This notebook shows how to make use of cuDF and cuGraph to do a Book Copurchase Graph Analysis.                                                                                                                             | SG | [amazon-books.csv] |
                                                                